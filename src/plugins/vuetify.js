import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '@mdi/font/css/materialdesignicons.css'
Vue.use(Vuetify,{

    icons: {
        iconfont: 'mdi', // default - only for display purposes
    },
    theme:{
        primary: '#1976D2',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#FFC107',
        warning: '#FFC107',
    }
});

export default new Vuetify({
});
