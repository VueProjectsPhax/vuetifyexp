import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path:'/verpersonas',
    name:'verpersonas',
    component: () => import('../views/VerPersonas.vue')
  },
  {
    path:'/agregarpersona',
    name:'agregarpersona',
    component: () => import('../views/AgregarPersona.vue')
  },
  {
    path:'/actualizarpersona',
    name:'actualizarpersona',
    component: () => import('../views/ActualizarPersona.vue')
  },
  {
    path:'/borrarpersona',
    name:'borrarpersona',
    component: () => import('../views/BorrarPersona.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
